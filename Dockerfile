# Use the official Solr image
FROM solr:9

# Copy custom configuration files to the container
#COPY path/to/solr-config /opt/solr/server/solr/configsets/myconfig
#COPY ./solr_cores /opt/solr/server/solr/mycores
#COPY loggingCore/conf /opt/solr/server/solr/configsets/loggingCore

#COPY ./configsets /configsets
COPY ./configsets /opt/solr/server/solr/configsets


# Set the default working directory
WORKDIR /opt/solr

# Expose the default Solr port
EXPOSE 8983

# Start Solr using a custom command
CMD ["solr", "start", "-f"]